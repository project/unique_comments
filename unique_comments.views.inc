<?php

/**
 * Implements hook_views_data()
 */
function unique_comments_views_data() {
dpm('peaches');
  $data = array(
    'unique_comments' => array(
      'table' => array(
        'group' => t('Unique Comments'),
        'title' => t('Unique Comments Status'),
        'help' => t('This field can be used in a view to show whether a node has Unique Comments enabled or disabled.'),
        'join' => array(
          'node' => array(
            'left_field' => 'nid',
            'field' => 'nid',
          ),
          'users' => array(
            'left_field' => 'uid',
            'field' => 'uid',
          ),
        ),
      ),
      'ucid' => array(
        'title' => t('Unique Comments ID'),
        'help' => t('something here'),
        'field' => array(
          'handler' => 'views_handler_field_numeric', 
          'click sortable' => TRUE,
        ), 
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ), 
        'sort' => array(
           'handler' => 'views_handler_sort',
        ),
      ),
      'nid' => array(
        'title' => t('NID'),
        'help' => t('The Node ID of the node.'),
        'relationship' => array(
          'base' => 'node', // The name of the table to join with 
          'field' => 'nid', // The name of the field to join with 
          'handler' => 'views_handler_relationship', 
          'label' => t('Example node'),
        ),
      ),
      'uid' => array(
        'title' => t('UID'),
        'help' => t('The User ID who altered the Unique Comments Status.'),
        'relationship' => array(
          'base' => 'users', // The name of the table to join with 
          'field' => 'uid', // The name of the field to join with 
          'handler' => 'views_handler_relationship',
          'label' => t('Example user'),
        ),
      ),
      'added' => array(
        'title' => t('Altered'),
        'help' => t('test'),
        'field' => array(
          'handler' => 'views_handler_field_date', 
          'click sortable' => TRUE, // This is use by the table display plugin.
        ), 
        'sort' => array(
          'handler' => 'views_handler_sort_date',
        ), 
        'filter' => array(
          'handler' => 'views_handler_filter_date',
        ), 
      ),
    ),
  );
  return $data;
}
